package tabuleiro;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import tabuleiro.arvore.No;
import tabuleiro.euristicas.TerceiraHeuristica;

public class TabuleiroFactoryTest {

	@Test
	public void factoryTabuleiroTest() throws IOException {
		String path = getClass().getResource("caso1").getPath();
		
		Tabuleiro tabuleiro = TabuleiroFactory.fromFile(path);
		assertNotNull("Deveria ter criado um tabuleiro", tabuleiro);
	}

	@Test
	public void testFilhos() throws FileNotFoundException {
		String path = getClass().getResource("caso1").getPath();

		Tabuleiro tabuleiro = TabuleiroFactory.fromFile(path);
		No no = new No(tabuleiro, null);
		no.geraFilhos();

		assertEquals("Número de filhos", 4, no.getFilhos().size());
		for (No filho : no.getFilhos())
			System.out.println(filho.getTabuleiro().toString());
	}
	
	@Test
	public void testHLinha2() throws FileNotFoundException{
		String path = getClass().getResource("caso1").getPath();

		Tabuleiro tabuleiro = TabuleiroFactory.fromFile(path);
		No no = new No(tabuleiro, null);
		
		new TerceiraHeuristica(no).calcular();
		System.out.println(no.gethLinha3());
	}
	
	@Test
	public void copiaTemOutraInstancia() throws FileNotFoundException{
		String path = getClass().getResource("caso1").getPath();
		
		Tabuleiro tabuleiro = TabuleiroFactory.fromFile(path);
		
		Tabuleiro copy = tabuleiro.copy();
		
		Assert.assertEquals(tabuleiro, copy);
		
		tabuleiro.moveCelulaNulaPara(Movimentador.BAIXO);
		
		Assert.assertFalse(tabuleiro.equals(copy));
	}
}