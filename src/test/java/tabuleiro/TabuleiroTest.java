package tabuleiro;

import java.io.FileNotFoundException;

import org.junit.Assert;
import org.junit.Test;

public class TabuleiroTest {
	
	@Test
	public void copiaTemOutraInstancia() throws FileNotFoundException{
		String path = getClass().getResource("caso1").getPath();
		
		Tabuleiro tabuleiro = TabuleiroFactory.fromFile(path);
		
		Tabuleiro copy = tabuleiro.copy();
		
		Assert.assertEquals(tabuleiro, copy);
		
		tabuleiro.moveCelulaNulaPara(Movimentador.BAIXO);
		
		Assert.assertFalse(tabuleiro.equals(copy));
	}

}
