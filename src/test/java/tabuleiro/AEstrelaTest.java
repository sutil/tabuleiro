package tabuleiro;

import java.io.FileNotFoundException;

import org.junit.Test;

public class AEstrelaTest {
	
	@Test
	public void testeAEstrela() throws FileNotFoundException{
		String path = getClass().getResource("caso1").getPath();
		Tabuleiro tabuleiro = TabuleiroFactory.fromFile(path);
		
		new AEstrela(tabuleiro).executar();
	}
	
	@Test
	public void testeAEstrelaB() throws FileNotFoundException{
		String path = getClass().getResource("caso2").getPath();
		Tabuleiro tabuleiro = TabuleiroFactory.fromFile(path);
		
		new AEstrela(tabuleiro).executar();
	}
	
	@Test
	public void testeAEstrelaC() throws FileNotFoundException{
		String path = getClass().getResource("caso3").getPath();
		Tabuleiro tabuleiro = TabuleiroFactory.fromFile(path);
		
		new AEstrela(tabuleiro).executar();
	}

}
