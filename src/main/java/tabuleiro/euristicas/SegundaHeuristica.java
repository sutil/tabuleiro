package tabuleiro.euristicas;

import java.util.Scanner;

import tabuleiro.arvore.No;

public class SegundaHeuristica extends Heuristica{

	public SegundaHeuristica(No no) {
		super(no);
	}

	@Override
	public void calcular() {
		int valor = 0;
		
		String sequencia = no.getTabuleiro().toString();
		
		Scanner scanner = new Scanner(sequencia);
		int anterior = scanner.nextInt();
		while (scanner.hasNext()){
			int proximo = scanner.nextInt();
			if((proximo -1) != anterior)
				valor++;
		}
		
		scanner.close();
		
		no.sethLinha2(valor);
	}
	
	
	/**
	 *  1  2  3  4
	 * 12 13 14  5
	 * 11  0 15  6
	 * 10  9  8  7
	 * */

}
