package tabuleiro.euristicas;

import tabuleiro.arvore.No;

public abstract class Heuristica {
	
	protected No no;

	public Heuristica(No no) {
		this.no = no;
	}
	
	public abstract void calcular();

}
