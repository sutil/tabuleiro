package tabuleiro.euristicas;

import tabuleiro.arvore.No;

public class QuartaHeuristica extends Heuristica{
	
	private static double p1 = 0.1;
	private static double p2 = 0.6;
	private static double p3 = 0.3;

	public QuartaHeuristica(No no) {
		super(no);
	}

	@Override
	public void calcular() {
		no.sethLinha4(
				no.gethLinha1()*p1 + 
				no.gethLinha3()*p3 +
				no.gethLinha2()*p2);
	}

}
