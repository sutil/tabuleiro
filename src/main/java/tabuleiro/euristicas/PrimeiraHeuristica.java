package tabuleiro.euristicas;

import tabuleiro.arvore.No;
import tabuleiro.celula.Celula;

public class PrimeiraHeuristica extends Heuristica{
	
	public PrimeiraHeuristica(No no){
		super(no);
	}
	
	@Override
	public void calcular(){
		int numeroDePecasForaDoLugar = 0;
		Celula[][] quadro = no.getTabuleiro().getQuadro();
		for(int i = 0; i < 4; i++)
			for(int j = 0; j < 4; j++)
				if(quadro[i][j].estaForaDoLugar())
					numeroDePecasForaDoLugar++;
		
		no.sethLinha1(numeroDePecasForaDoLugar);;
	}

}
