package tabuleiro.euristicas;

import tabuleiro.Tabuleiro;
import tabuleiro.arvore.No;
import tabuleiro.celula.Celula;

public class TerceiraHeuristica extends Heuristica{
	
	public TerceiraHeuristica(No no){
		super(no);
	}
	
	@Override
	public void calcular(){
		int valor = 0;
		Tabuleiro tabuleiro = no.getTabuleiro();
		for(int i = 0; i< 4; i++)
			for(int j = 0; j< 4; j++)
				valor += calculaDistanciaRetangular(tabuleiro.getCelula(i, j));
		
		no.sethLinha3(valor);
	}
	
	private int calculaDistanciaRetangular(Celula celula) {
		return distanciaEmAltura(celula) + distanciaEmLargura(celula);
	}

	private int distanciaEmLargura(Celula celula) {
		int distancia = celula.getPosicaoJ() - celula.posicaoCorretaJ();
		if(distancia < 0)
			return celula.posicaoCorretaJ() - celula.getPosicaoJ();
		
		return distancia;
	}

	private int distanciaEmAltura(Celula celula) {
		int distancia = celula.getPosicaoI() - celula.posicaoCorretaI();
		if(distancia < 0)
			return celula.posicaoCorretaI() - celula.getPosicaoI();
		
		return distancia;
	}

}
