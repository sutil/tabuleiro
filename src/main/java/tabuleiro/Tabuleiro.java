package tabuleiro;

import tabuleiro.celula.Celula;
import tabuleiro.celula.CelulaNula;

public class Tabuleiro {
	
	protected Celula quadro[][];
	
	private Tabuleiro(Celula quadro[][]){
		this.quadro = quadro;
	}
	
	protected Tabuleiro(){}
	
	public static Tabuleiro newInstance(final Celula quadro[][]){
		if(quadro == null)
			throw new RuntimeException("Quadro deve ser informado");
		
		if(quadro.length != 4 || quadro[0].length != 4)
			throw new RuntimeException("Tabuleiro deve ter 4 linhas e 4 colunas");
		
		return new Tabuleiro(quadro);
	}
	
	public Celula[][] getQuadro() {
		return quadro;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Tabuleiro){
			Tabuleiro other = (Tabuleiro)obj;
			for(int i = 0; i<4; i++)
				for(int j = 0; j<4; j++)
					if(!this.getCelula(i, j).equals(other.getCelula(i, j)))
						return false;
			
			return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for(int i = 0; i<4; i++){
			for(int j = 0; j<4; j++){
				builder.append(quadro[i][j]).append(" ");
			}
			builder.append(System.getProperty("line.separator"));
		}
		return builder.toString();
	}
	
	public Celula getCelula(int i, int j){
		return quadro[i][j];
	}

	public Tabuleiro copy() {
		return TabuleiroFactory.copy(this);
	}

	public CelulaNula getCelulaNula() {
		for(int i = 0; i<4; i++)
			for(int j = 0; j<4; j++)
				if(quadro[i][j].isCelulaNula())
					return (CelulaNula)quadro[i][j];
		
		return null;
	}

	public void atualizaPeca(Celula celula) {
		this.quadro[celula.getPosicaoI()][celula.getPosicaoJ()] = celula;
	}

	public void moveCelulaNulaPara(Movimentador movimento) {
		movimento.mover(this);
	}

}
