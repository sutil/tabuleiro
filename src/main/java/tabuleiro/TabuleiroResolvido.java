package tabuleiro;

import tabuleiro.celula.Celula;
import tabuleiro.celula.CelulaFactory;

public class TabuleiroResolvido extends Tabuleiro{
	
	private static TabuleiroResolvido tabuleiroResolvido = new TabuleiroResolvido();
	
	public TabuleiroResolvido() {
		Celula c1 = CelulaFactory.newInstance(1, 0, 0);
		Celula c2 = CelulaFactory.newInstance(2, 0, 1);
		Celula c3 = CelulaFactory.newInstance(3, 0, 2);
		Celula c4 = CelulaFactory.newInstance(4, 0, 3);
		
		Celula c5 = CelulaFactory.newInstance(12, 1, 0);
		Celula c6 = CelulaFactory.newInstance(13, 1, 1);
		Celula c7 = CelulaFactory.newInstance(14, 1, 2);
		Celula c8 = CelulaFactory.newInstance(5, 1, 3);
		
		Celula c9 = CelulaFactory.newInstance(11, 2, 0);
		Celula c10 = CelulaFactory.newInstance(16, 2, 1);
		Celula c11 = CelulaFactory.newInstance(15, 2, 2);
		Celula c12 = CelulaFactory.newInstance(6, 2, 3);
		
		Celula c13 = CelulaFactory.newInstance(10, 3, 0);
		Celula c14 = CelulaFactory.newInstance(9, 3, 1);
		Celula c15 = CelulaFactory.newInstance(8, 3, 2);
		Celula c16 = CelulaFactory.newInstance(7, 3, 3);
		
		
		
		this.quadro  = new Celula[][]{
				{c1 ,c2 ,c3 ,c4 },
				{c5 ,c6 ,c7 ,c8 },
				{c9 ,c10,c11,c12},
				{c13,c14,c15,c16}
				};
	}

	public static TabuleiroResolvido getInstance() {
		return tabuleiroResolvido;
	}

}
