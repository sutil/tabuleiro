package tabuleiro;


public enum Movimentador {

	CIMA {
		@Override
		public void mover(Tabuleiro tabuleiro) {
			tabuleiro.getCelulaNula().moveParaCima(tabuleiro);
		}
	},
	BAIXO {
		@Override
		public void mover(Tabuleiro tabuleiro) {
			tabuleiro.getCelulaNula().moveParaBaixo(tabuleiro);
		}
	},
	DIREITA {
		@Override
		public void mover(Tabuleiro tabuleiro) {
			tabuleiro.getCelulaNula().moveParaDireita(tabuleiro);
		}
	},
	ESQUERDA {
		@Override
		public void mover(Tabuleiro tabuleiro) {
			tabuleiro.getCelulaNula().moveParaEsquerda(tabuleiro);
		}
	};

	public abstract void mover(Tabuleiro tabuleiro);

}
