package tabuleiro.celula;

import tabuleiro.Tabuleiro;

public class CelulaNula extends Celula{

	public CelulaNula(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 2;
	}

	@Override
	public int posicaoCorretaJ() {
		return 1;
	}

	@Override
	public int getValor() {
		return 0;
	}
	
	public void moveParaCima(Tabuleiro tabuleiro){
		if(posicaoI > 0)
			atualizaCelulas(tabuleiro, posicaoI - 1, posicaoJ);
	}
	
	public void moveParaBaixo(Tabuleiro tabuleiro){
		if(posicaoI < 3)
			atualizaCelulas(tabuleiro, posicaoI + 1, posicaoJ);
	}
	
	public void moveParaDireita(Tabuleiro tabuleiro){
		if(posicaoJ < 3)
			atualizaCelulas(tabuleiro, posicaoI, posicaoJ + 1);
	}
	
	public void moveParaEsquerda(Tabuleiro tabuleiro){
		if(posicaoJ > 0)
			atualizaCelulas(tabuleiro, posicaoI, posicaoJ - 1);
			
	}
	
	public void atualizaCelulas(Tabuleiro tabuleiro, int i, int j){
		Celula celula = tabuleiro.getCelula(i, j);
		trocaCom(celula);
		tabuleiro.atualizaPeca(this);
		tabuleiro.atualizaPeca(celula);
	}
	
	private void trocaCom(Celula outra){
		if(outra != this){
			int i = outra.getPosicaoI();
			int j = outra.getPosicaoJ();
			outra.vaiParaLugarQueEstavaNulo(this);
			this.posicaoI = i;
			this.posicaoJ = j;
		}
	}
	
	@Override
	public boolean isCelulaNula() {
		return true;
	}

}
