package tabuleiro.celula;

public class Celula15 extends Celula{

	public Celula15(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 2;
	}

	@Override
	public int posicaoCorretaJ() {
		return 2;
	}

	@Override
	public int getValor() {
		return 15;
	}

}
