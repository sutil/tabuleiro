package tabuleiro.celula;

public class Celula13 extends Celula{


	public Celula13(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 1;
	}

	@Override
	public int posicaoCorretaJ() {
		return 1;
	}

	@Override
	public int getValor() {
		return 13;
	}

}
