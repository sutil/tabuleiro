package tabuleiro.celula;

public class Celula3 extends Celula{

	public Celula3(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 0;
	}

	@Override
	public int posicaoCorretaJ() {
		return 2;
	}

	@Override
	public int getValor() {
		return 3;
	}

}
