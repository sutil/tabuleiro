package tabuleiro.celula;

public class Celula10 extends Celula{


	public Celula10(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 3;
	}

	@Override
	public int posicaoCorretaJ() {
		return 0;
	}

	@Override
	public int getValor() {
		return 10;
	}

}
