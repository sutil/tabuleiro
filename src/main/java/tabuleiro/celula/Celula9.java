package tabuleiro.celula;

public class Celula9 extends Celula{

	public Celula9(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 3;
	}

	@Override
	public int posicaoCorretaJ() {
		return 1;
	}

	@Override
	public int getValor() {
		return 9;
	}

}
