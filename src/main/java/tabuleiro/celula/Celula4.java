package tabuleiro.celula;

public class Celula4 extends Celula{

	public Celula4(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 0;
	}

	@Override
	public int posicaoCorretaJ() {
		return 3;
	}

	@Override
	public int getValor() {
		return 4;
	}

}
