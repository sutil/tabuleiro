package tabuleiro.celula;

public class Celula12 extends Celula{


	public Celula12(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 1;
	}

	@Override
	public int posicaoCorretaJ() {
		return 0;
	}

	@Override
	public int getValor() {
		return 12;
	}

}
