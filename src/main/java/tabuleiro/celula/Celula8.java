package tabuleiro.celula;

public class Celula8 extends Celula{

	public Celula8(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 3;
	}

	@Override
	public int posicaoCorretaJ() {
		return 2;
	}

	@Override
	public int getValor() {
		return 8;
	}

}
