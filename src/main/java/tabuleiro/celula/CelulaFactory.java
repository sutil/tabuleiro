package tabuleiro.celula;

public class CelulaFactory {

	public static Celula newInstance(int valor, int posicaoI, int posicaoJ) {
		switch (valor) {
		case 1:
			return new Celula1(posicaoI, posicaoJ);
		case 2:
			return new Celula2(posicaoI, posicaoJ);
		case 3:
			return new Celula3(posicaoI, posicaoJ);
		case 4:
			return new Celula4(posicaoI, posicaoJ);
		case 5:
			return new Celula5(posicaoI, posicaoJ);
		case 6:
			return new Celula6(posicaoI, posicaoJ);
		case 7:
			return new Celula7(posicaoI, posicaoJ);
		case 8:
			return new Celula8(posicaoI, posicaoJ);
		case 9:
			return new Celula9(posicaoI, posicaoJ);
		case 10:
			return new Celula10(posicaoI, posicaoJ);
		case 11:
			return new Celula11(posicaoI, posicaoJ);
		case 12:
			return new Celula12(posicaoI, posicaoJ);
		case 13:
			return new Celula13(posicaoI, posicaoJ);
		case 14:
			return new Celula14(posicaoI, posicaoJ);
		case 15:
			return new Celula15(posicaoI, posicaoJ);
		default:
			return new CelulaNula(posicaoI, posicaoJ);
		}
	}

}
