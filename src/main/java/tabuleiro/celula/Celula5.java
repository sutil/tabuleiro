package tabuleiro.celula;

public class Celula5 extends Celula{

	public Celula5(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 1;
	}

	@Override
	public int posicaoCorretaJ() {
		return 3;
	}

	@Override
	public int getValor() {
		return 5;
	}

}
