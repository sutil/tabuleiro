package tabuleiro.celula;

public class Celula11 extends Celula{


	public Celula11(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 2;
	}

	@Override
	public int posicaoCorretaJ() {
		return 0;
	}

	@Override
	public int getValor() {
		return 11;
	}

}
