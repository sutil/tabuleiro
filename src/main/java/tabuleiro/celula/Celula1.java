package tabuleiro.celula;

public class Celula1 extends Celula{

	public Celula1(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 0;
	}

	@Override
	public int posicaoCorretaJ() {
		return 0;
	}

	@Override
	public int getValor() {
		return 1;
	}

}
