package tabuleiro.celula;

public class Celula7 extends Celula{

	public Celula7(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 3;
	}

	@Override
	public int posicaoCorretaJ() {
		return 3;
	}

	@Override
	public int getValor() {
		return 7;
	}

}
