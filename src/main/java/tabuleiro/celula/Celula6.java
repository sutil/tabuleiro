package tabuleiro.celula;

public class Celula6 extends Celula{

	public Celula6(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 2;
	}

	@Override
	public int posicaoCorretaJ() {
		return 3;
	}

	@Override
	public int getValor() {
		return 6;
	}

}
