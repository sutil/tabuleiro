package tabuleiro.celula;

public class Celula2 extends Celula{

	public Celula2(int posicaoI, int posicaoJ) {
		super(posicaoI, posicaoJ);
	}

	@Override
	public int posicaoCorretaI() {
		return 0;
	}

	@Override
	public int posicaoCorretaJ() {
		return 1;
	}

	@Override
	public int getValor() {
		return 2;
	}

}
