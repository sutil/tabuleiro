package tabuleiro.celula;


public abstract class Celula {
	
	protected int posicaoI;
	protected int posicaoJ;
	
	public Celula(int posicaoI, int posicaoJ){
		this.posicaoI = posicaoI;
		this.posicaoJ = posicaoJ;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Celula){
			Celula other = (Celula) obj;
			return this.getValor() == other.getValor();
		}
		return false;
	}
	
	@Override
	public String toString() {
		return String.valueOf(getValor());
	}

	public boolean estaForaDoLugar() {
		return posicaoI != posicaoCorretaI() || posicaoJ != posicaoCorretaJ();
	}
	
	public int getPosicaoI() {
		return posicaoI;
	}
	
	public int getPosicaoJ() {
		return posicaoJ;
	}
	
	public void vaiParaLugarQueEstavaNulo(CelulaNula nula){
		this.posicaoI = nula.getPosicaoI();
		this.posicaoJ = nula.getPosicaoJ();
	}
	
	public Celula copy(){
		return CelulaFactory.newInstance(getValor(), posicaoI, posicaoJ);
	}
	
	public boolean isCelulaNula() {
		return false;
	}
	
	public abstract int getValor();
	
	public abstract int posicaoCorretaI();
	
	public abstract int posicaoCorretaJ();

}
