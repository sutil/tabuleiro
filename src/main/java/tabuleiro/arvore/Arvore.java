package tabuleiro.arvore;

import tabuleiro.Tabuleiro;

public class Arvore {
	
	private No raiz;
	
	public Arvore(Tabuleiro tabuleiro){
		this.raiz = new No(tabuleiro, null);
	}
	
	public No getRaiz() {
		return raiz;
	}

}
