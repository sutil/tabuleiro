package tabuleiro.arvore;

import static tabuleiro.Movimentador.BAIXO;
import static tabuleiro.Movimentador.CIMA;
import static tabuleiro.Movimentador.DIREITA;
import static tabuleiro.Movimentador.ESQUERDA;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Objects;

import tabuleiro.Movimentador;
import tabuleiro.Tabuleiro;
import tabuleiro.TabuleiroResolvido;

public class No implements Comparable<No> {

	private No pai;
	private List<No> filhos = new ArrayList<No>();

	private final Tabuleiro tabuleiro;

	private int hLinha1 = 0;
	private int hLinha3 = 0;
	private double hLinha4 = 0;
	private int hLinha2 = 0;

	public No(Tabuleiro tabuleiro, No pai) {
		this.tabuleiro = tabuleiro;
		this.pai = pai;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof No){
			No other = (No)obj;
			return Objects.equal(this.tabuleiro, other.tabuleiro);
		}
		return false;
	}
	
	@Override
	public String toString() {
		return String.valueOf(getF());
	}
	
	@Override
	public int compareTo(No o) {
		if(this.getF() <= o.getF())
			return -1;
		return 1;
	}
	
	public List<No> getFilhos() {
		return filhos;
	}

	public No getPai() {
		return pai;
	}

	public Tabuleiro getTabuleiro() {
		return tabuleiro;
	}

	public void sethLinha1(int hLinha1) {
		this.hLinha1 = hLinha1;
	}

	public int gethLinha1() {
		return hLinha1;
	}
	
	public void sethLinha3(int hLinha3) {
		this.hLinha3  = hLinha3;
	}
	
	public int gethLinha3() {
		return hLinha3;
	}
	
	public int gethLinha2() {
		return hLinha2;
	}
	
	public double getF(){
		return custoMenorCaminhoAteRaiz()+
				hLinha1+
				hLinha2+
				hLinha3+
				hLinha4;
	}

	public int custoMenorCaminhoAteRaiz() {
		if (pai == null)
			return 0;

		return getPai().custoMenorCaminhoAteRaiz() + 1;
	}

	public void geraFilhos() {
		geraFilho(BAIXO);
		geraFilho(CIMA);
		geraFilho(DIREITA);
		geraFilho(ESQUERDA);
	}

	public String chave() {
		return tabuleiro.toString();
	}

	public boolean isResultadoFinal() {
		return this.tabuleiro.equals(TabuleiroResolvido.getInstance());
	}

	public void sethLinha4(double d) {
		this.hLinha4  = d;
	}
	
	public double gethLinha4() {
		return hLinha4;
	}

	private void geraFilho(Movimentador movimento) {
		Tabuleiro tabulero = tabuleiro.copy();
		tabulero.moveCelulaNulaPara(movimento);
		if (!tabulero.equals(this.tabuleiro) &&  !tabuleiroIgualDoPai(tabulero))
			filhos.add(new No(tabulero, this));
	}

	private boolean tabuleiroIgualDoPai(Tabuleiro tabuleiro){
		return getPai() != null && tabuleiro.equals(getPai().getTabuleiro());
	}

	public void sethLinha2(int valor) {
		this.hLinha2 = valor;
		
	}

	

}
