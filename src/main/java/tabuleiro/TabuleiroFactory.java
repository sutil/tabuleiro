package tabuleiro;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

import tabuleiro.celula.Celula;
import tabuleiro.celula.CelulaFactory;

public class TabuleiroFactory {
	
	public static Tabuleiro fromFile(String path) throws FileNotFoundException{
		FileInputStream file = new FileInputStream(path);
		Scanner scanner = new Scanner(file);
		
		Celula[][] quadro = new Celula[4][4];
		for(int i = 0; i < 4; i++)
			for(int j = 0; j < 4; j++)
				quadro[i][j] = CelulaFactory.newInstance(scanner.nextInt(), i, j);
		
		scanner.close();
		return Tabuleiro.newInstance(quadro);
	}
	
	public static Tabuleiro copy(Tabuleiro tabuleiro){
		Celula[][] quadroNovo = new Celula[4][4];
		for(int i = 0; i < 4; i++)
			for(int j = 0; j < 4; j++){
				Celula celula = tabuleiro.getCelula(i, j).copy();
				quadroNovo[i][j] = celula;
			}
		return Tabuleiro.newInstance(quadroNovo);
	}

}
