package tabuleiro;

import java.util.Comparator;
import java.util.Map;
import java.util.PriorityQueue;

import tabuleiro.arvore.Arvore;
import tabuleiro.arvore.No;
import tabuleiro.euristicas.PrimeiraHeuristica;
import tabuleiro.euristicas.QuartaHeuristica;
import tabuleiro.euristicas.SegundaHeuristica;
import tabuleiro.euristicas.TerceiraHeuristica;

import com.google.common.collect.Maps;

public class AEstrela {

	private PriorityQueue<No> fila = fila();
	private Map<String, No> abertos = Maps.newHashMap();
	private Map<String, No> fechados = Maps.newHashMap();

	private final Arvore arvore;
	private No resultado;

	public AEstrela(Tabuleiro tabuleiro) {
		arvore = new Arvore(tabuleiro);
	}

	public void executar() {
		// A
		No no = arvore.getRaiz();
		calculaHeuristicas(no);
		adicionaAosAbertos(no);
		teste();

	}

	private void teste() {
		while (resultado == null) {
			
			if (abertos.isEmpty())
				throw new RuntimeException("Fracasso");
			
			No v = menorFDosAbertos();
			fechados.put(v.chave(), v);

			//c
			if (v.isResultadoFinal()){
				resultado = v;
				System.out.println(v.custoMenorCaminhoAteRaiz());
				return;
			}

			v.geraFilhos();
			
			if (v.getFilhos().isEmpty())
				continue;

			// d
			for (No m : v.getFilhos()) {
				if (!noExistente(m) || noComGMenorQueExistente(m)) {
					calculaHeuristicas(m);
					adicionaAosAbertos(m);
					removeDosFechados(m);
				}
			}

		}
	}

	private void calculaHeuristicas(No m) {
		new PrimeiraHeuristica(m).calcular();
		new SegundaHeuristica(m).calcular();
		new TerceiraHeuristica(m).calcular();
		new QuartaHeuristica(m).calcular();
	}

	private void removeDosFechados(No m) {
		fechados.remove(m.chave());
	}

	private void adicionaAosAbertos(No m) {
		if(abertos.containsKey(m.chave()))
			removeDosAbertos(m);

		abertos.put(m.chave(), m);
		fila.add(m);
	}
	

	private boolean noComGMenorQueExistente(No m) {
		No existente = abertos.get(m.chave());
		return existente != null
				&& m.custoMenorCaminhoAteRaiz() <= existente
						.custoMenorCaminhoAteRaiz();
	}

	private boolean noExistente(No m) {
		return abertos.containsKey(m.chave())
				|| fechados.containsKey(m.chave());
	}

	private No menorFDosAbertos() {
		No no = fila.remove();
		abertos.remove(no.chave());
		return no;	
	}
	
	private void removeDosAbertos(No no){
		abertos.remove(no.chave());
		fila.remove(no);
	}
	
	private static PriorityQueue<No> fila(){
		int capacity = 99999999;
		
		Comparator<No> comparator = new Comparator<No>() {

			@Override
			public int compare(No o1, No o2) {
				return o1.getF() <= o2.getF() ? -1 : 1;
			}
		};
		
		return new PriorityQueue<No>(capacity, comparator);
	}


}
